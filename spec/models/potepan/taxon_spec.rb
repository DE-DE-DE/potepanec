describe Spree::Taxon, type: :model do
  let(:categories_taxonomy) { create(:taxonomy, name: 'Categories') }
  let(:clothing_taxon) { create(:taxon, name: 'Clothing', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }
  let(:brands_taxonomy) { create(:taxonomy, name: 'Brands') }
  let(:rails_taxon) { create(:taxon, name: 'Ruby on Rails', taxonomy: brands_taxonomy, parent_id: brands_taxonomy.root.id) }
  let(:shirt) { create(:custom_product, name: 'Ruby on Rails Ringer T-Shirt', price: '19.99', taxons: [rails_taxon, clothing_taxon]) }

  context '#taxon_id_with_categories' do
    it "与えられたproductに紐づくTaxonの内、Categoriesという名前のTaxonomyに紐づくもののIDを取得できること" do
      expect(Spree::Taxon.taxon_id_with_categories(shirt)).to eq clothing_taxon.id
    end
  end
end
