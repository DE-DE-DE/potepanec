describe Spree::Product, type: :model do
  let(:categories_taxonomy) { create(:taxonomy, name: 'Categories') }
  let!(:mugs_taxon) { create(:taxon, name: 'Mugs', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }
  let!(:bags_taxon) { create(:taxon, name: 'Bags', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }

  let(:brands_taxonomy) { create(:taxonomy, name: 'Brands') }
  let!(:rails_taxon) { create(:taxon, name: 'Rails', taxonomy: brands_taxonomy, parent_id: brands_taxonomy.root.id) }
  let!(:ruby_taxon) { create(:taxon, name: 'Ruby', taxonomy: brands_taxonomy, parent_id: brands_taxonomy.root.id) }

  let(:tote) { create(:custom_product, name: 'Rails Tote', price: '19.99', taxons: [rails_taxon, bags_taxon]) }

  describe '#related_products' do
    context '１つのtaxonにつき、それぞれ２つの商品が紐づいているとき' do
      let!(:backpack) { create(:custom_product, name: 'Rails Backpack', price: '18.99', taxons: [rails_taxon, bags_taxon]) }
      let!(:rails_mug) { create(:custom_product, name: 'Rails Mug', price: '17.99', taxons: [rails_taxon, mugs_taxon]) }
      let!(:shoulder) { create(:custom_product, name: 'Ruby Shoulder', price: '16.99', taxons: [ruby_taxon, bags_taxon]) }
      let!(:ruby_mug) { create(:custom_product, name: 'Ruby Mug', price: '15.99', taxons: [ruby_taxon, mugs_taxon]) }

      it "RailsカテゴリとBagsカテゴリをもつ商品を取得できること" do
        expect(tote.related_products).to include backpack
      end

      it "RailsカテゴリかBagsカテゴリのいずれかをもつ商品(rails_mug)を取得できること" do
        expect(tote.related_products).to include rails_mug
      end

      it "RailsカテゴリかBagsカテゴリのいずれかをもつ商品(shoulder)を取得できること" do
        expect(tote.related_products).to include shoulder
      end

      it "RailsカテゴリとBagsカテゴリのいずれでもない商品を取得しないこと" do
        expect(tote.related_products).not_to include ruby_mug
      end

      it "呼び出し元の商品toteが含まれていないこと" do
        expect(tote.related_products).not_to include tote
      end
    end
  end
end
