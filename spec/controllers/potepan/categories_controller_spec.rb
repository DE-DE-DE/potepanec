RSpec.describe Potepan::CategoriesController, type: :controller do
  describe '#show' do
    let(:categories_taxonomy) { create(:taxonomy, name: 'Categories') }
    let(:clothing_taxon) { create(:taxon, name: 'Clothing', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }
    let!(:shirt) { create(:custom_product, name: 'Ruby on Rails Ringer T-Shirt', price: '19.99', taxons: [clothing_taxon]) }

    it "200レスポンスを返すこと" do
      get :show, params: { id: clothing_taxon.id }
      expect(response).to have_http_status "200"
    end

    it "コントローラのインスタンス変数@taxonに正しい値が入っていること" do
      get :show, params: { id: clothing_taxon.id }
      expect(assigns(:taxon)).to eq clothing_taxon
    end

    it "コントローラのインスタンス変数@taxonomiesに正しい値が入っていること" do
      get :show, params: { id: clothing_taxon.id }
      expect(assigns(:taxonomies)).to match_array([categories_taxonomy])
    end

    it "コントローラのインスタンス変数@productsに正しい値が入っていること" do
      get :show, params: { id: clothing_taxon.id }
      expect(assigns(:products)).to match_array([shirt])
    end
  end
end
