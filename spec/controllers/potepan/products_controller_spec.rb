RSpec.describe Potepan::ProductsController, type: :controller do
  describe '#show' do
    let(:categories_taxonomy) { create(:taxonomy, name: 'Categories') }
    let(:clothing_taxon) { create(:taxon, name: 'Clothing', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }
    let(:mugs_taxon) { create(:taxon, name: 'Mugs', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }
    let(:bags_taxon) { create(:taxon, name: 'Bags', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }

    let(:brands_taxonomy) { create(:taxonomy, name: 'Brands') }
    let(:rails_taxon) { create(:taxon, name: 'Ruby on Rails', taxonomy: brands_taxonomy, parent_id: brands_taxonomy.root.id) }
    let(:ruby_taxon) { create(:taxon, name: 'Ruby', taxonomy: brands_taxonomy, parent_id: brands_taxonomy.root.id) }

    let(:shirt) { create(:custom_product, name: 'Ruby on Rails Ringer T-Shirt', price: '19.99', taxons: [rails_taxon, clothing_taxon]) }
    let!(:jersey) { create(:custom_product, name: 'Ruby on Rails Baseball Jersey', price: '18.99', taxons: [rails_taxon, clothing_taxon]) }
    let!(:mug) { create(:custom_product, name: 'Ruby on Rails Baseball Stein', price: '17.99', taxons: [rails_taxon, mugs_taxon]) }
    let!(:uniform) { create(:custom_product, name: 'Ruby Football Uniform', price: '16.99', taxons: [ruby_taxon, clothing_taxon]) }
    let!(:bag) { create(:custom_product, name: 'Ruby Bag', price: '15.99', taxons: [ruby_taxon, bags_taxon]) }

    before do
      get :show, params: { id: shirt.id }
    end

    it "200レスポンスを返すこと" do
      expect(response).to have_http_status "200"
    end

    it "コントローラのインスタンス変数@productに正しい値が入っていること" do
      expect(assigns(:product)).to eq shirt
    end

    it "コントローラのインスタンス変数@related_productsに正しい値が入っていること" do
      get :show, params: { id: shirt.id }
      expect(assigns(:related_products)).to match_array([uniform, mug, jersey])
    end
  end
end
