describe ApplicationHelper do
  describe 'タイトル生成するヘルパーメソッドが正常動作すること' do
    subject { helper.full_title(title) }

    base_title = "BIGBAG Store"

    context '引数がTitleのとき "Title - BIGBAG Store" と表示' do
      let(:title) { "Title" }

      it { is_expected.to eq "Title - #{base_title}" }
    end

    context '引数が空白のとき "BIGBAG Store" と表示' do
      let(:title) { "" }

      it { is_expected.to eq base_title }
    end

    context '引数がnilのとき "BIGBAG Store" と表示' do
      let(:title) { nil }

      it { is_expected.to eq base_title }
    end
  end
end
