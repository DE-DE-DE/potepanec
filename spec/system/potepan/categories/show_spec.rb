describe 'カテゴリ別商品ページ', type: :system do
  let(:categories_taxonomy) { create(:taxonomy, name: 'Categories') }
  let(:clothing_taxon) { create(:taxon, name: 'Clothing', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }
  let(:mugs_taxon) { create(:taxon, name: 'Mugs', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }
  let(:brands_taxonomy) { create(:taxonomy, name: 'Brands') }
  let(:rails_taxon) { create(:taxon, name: 'Ruby on Rails', taxonomy: brands_taxonomy, parent_id: brands_taxonomy.root.id) }
  let!(:shirt) { create(:custom_product, name: 'Ruby on Rails Ringer T-Shirt', price: '19.99', taxons: [rails_taxon, clothing_taxon]) }
  let!(:mug) { create(:custom_product, name: 'Ruby on Rails Mug', price: '15.99', taxons: [rails_taxon, mugs_taxon]) }

  before do
    visit potepan_category_path(clothing_taxon.id)
  end

  context '画面表示' do
    it 'ページタイトルが正しいこと' do
      expect(page).to have_title "#{clothing_taxon.name} - BIGBAG Store"
    end

    it '商品情報が表示されること' do
      expect(find('.page-title')).to have_content clothing_taxon.name
      expect(find('.breadcrumb')).to have_content clothing_taxon.name
      within ".mainContent" do
        expect(page).to have_content shirt.name
        expect(page).to have_content shirt.display_price
      end
    end

    it 'カテゴリ名が表示されること' do
      expect(page).to have_content clothing_taxon.name
      expect(page).to have_content mugs_taxon.name
      expect(page).to have_content rails_taxon.name
    end
  end

  context 'リンク' do
    it 'カテゴリ名クリックでページ遷移、商品表示が更新されること' do
      within ".side-nav" do
        click_link 'Mugs'
      end
      expect(current_path).to eq potepan_category_path(mugs_taxon.id)
      expect(find('.page-title')).to have_content mugs_taxon.name
      expect(find('.breadcrumb')).to have_content mugs_taxon.name
      within ".mainContent" do
        expect(page).to have_content mug.name
        expect(page).to have_content mug.display_price
      end
    end
  end
end
