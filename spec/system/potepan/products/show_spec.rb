describe '商品ページ', type: :system do
  let(:categories_taxonomy) { create(:taxonomy, name: 'Categories') }
  let(:mugs_taxon) { create(:taxon, name: 'Mugs', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }
  let(:bags_taxon) { create(:taxon, name: 'Bags', taxonomy: categories_taxonomy, parent_id: categories_taxonomy.root.id) }

  let(:brands_taxonomy) { create(:taxonomy, name: 'Brands') }
  let(:rails_taxon) { create(:taxon, name: 'Rails', taxonomy: brands_taxonomy, parent_id: brands_taxonomy.root.id) }
  let(:ruby_taxon) { create(:taxon, name: 'Ruby', taxonomy: brands_taxonomy, parent_id: brands_taxonomy.root.id) }

  let(:tote) { create(:custom_product, name: 'Rails Tote', price: '19.99', taxons: [rails_taxon, bags_taxon]) }

  describe 'タイトルとヘッダー部分' do
    before do
      visit potepan_product_path(tote.id)
    end

    it 'ページタイトルが正しいこと' do
      expect(page).to have_title "#{tote.name} - BIGBAG Store"
    end

    it 'トップページへリンク' do
      within ".navbar-header" do
        click_on 'logo'
      end
      expect(current_path).to eq potepan_path
    end
  end

  describe 'メイン表示している商品' do
    before do
      visit potepan_product_path(tote.id)
    end

    it 'メイン商品が表示されること' do
      expect(find('.page-title')).to have_content tote.name
      expect(find('.breadcrumb')).to have_content tote.name
      within ".media-body" do
        expect(page).to have_content tote.name
        expect(page).to have_content tote.display_price
        expect(page).to have_content tote.description
      end
    end

    it 'メイン商品のカテゴリページへリンク' do
      within ".media-body" do
        click_on '一覧ページへ戻る'
      end
      expect(current_path).to eq potepan_category_path(bags_taxon.id)
    end
  end

  describe '関連商品' do
    context '関連商品が４つ以下の場合' do
      let!(:backpack) { create(:custom_product, name: 'Rails Backpack', price: '18.99', taxons: [rails_taxon, bags_taxon]) }
      let!(:rails_mug) { create(:custom_product, name: 'Rails Mug', price: '17.99', taxons: [rails_taxon, mugs_taxon]) }
      let!(:shoulder) { create(:custom_product, name: 'Ruby Shoulder', price: '16.99', taxons: [ruby_taxon, bags_taxon]) }
      let!(:ruby_mug) { create(:custom_product, name: 'Ruby Mug', price: '15.99', taxons: [ruby_taxon, mugs_taxon]) }

      before do
        visit potepan_product_path(tote.id)
      end

      it 'RailsカテゴリとBagsカテゴリをもつ商品が関連商品として表示されること' do
        within "#related_products" do
          expect(page).to have_content backpack.name
          expect(page).to have_content backpack.display_price
        end
      end

      it 'RailsカテゴリかBagsカテゴリのいずれかをもつ商品が関連商品として表示されること' do
        within "#related_products" do
          expect(page).to have_content rails_mug.name
          expect(page).to have_content rails_mug.display_price
          expect(page).to have_content shoulder.name
          expect(page).to have_content shoulder.display_price
        end
      end

      it 'BagsカテゴリとRailsカテゴリのいずれでもない商品が関連商品に表示されないこと' do
        within "#related_products" do
          expect(page).not_to have_content ruby_mug.name
          expect(page).not_to have_content ruby_mug.display_price
        end
      end

      it 'メイン商品が関連商品に含まれていないこと' do
        within "#related_products" do
          expect(page).not_to have_content tote.name
          expect(page).not_to have_content tote.display_price
        end
      end

      it '関連商品をクリックするとその商品ページへリンクすること' do
        within "#related_product2" do
          click_on 'products-img'
        end
        expect(current_path).to eq potepan_product_path(backpack.id)
      end
    end

    context '関連商品が５つ以上の場合' do
      let!(:related_tote1) { create(:custom_product, name: 'Rails Tote1', price: '11.00', taxons: [rails_taxon, bags_taxon]) }
      let!(:related_tote2) { create(:custom_product, name: 'Rails Tote2', price: '12.00', taxons: [rails_taxon, bags_taxon]) }
      let!(:related_tote3) { create(:custom_product, name: 'Rails Tote3', price: '13.00', taxons: [rails_taxon, bags_taxon]) }
      let!(:related_tote4) { create(:custom_product, name: 'Rails Tote4', price: '14.00', taxons: [rails_taxon, bags_taxon]) }
      let!(:related_tote5) { create(:custom_product, name: 'Rails Tote5', price: '15.00', taxons: [rails_taxon, bags_taxon]) }

      before do
        visit potepan_product_path(tote.id)
      end

      it '更新日時の新しい４つの関連商品が表示されること' do
        within "#related_products" do
          related_totes = [related_tote2, related_tote3, related_tote4, related_tote5]
          related_totes.each do |tote|
            expect(page).to have_content tote.name
            expect(page).to have_content tote.display_price
          end
        end
      end

      it '更新日時の古い(新しい順で5番目以降の)関連商品が表示されないこと' do
        within "#related_products" do
          expect(page).not_to have_content related_tote1.name
          expect(page).not_to have_content related_tote1.display_price
        end
      end
    end
  end
end
