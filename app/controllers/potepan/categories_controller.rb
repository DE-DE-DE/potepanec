class Potepan::CategoriesController < Potepan::SampleController
  def show
    @taxonomies = Spree::Taxonomy.all
    @taxon = Spree::Taxon.find(params[:id])
    @products = @taxon.products.includes(master: [:images, :default_price])
  end
end
