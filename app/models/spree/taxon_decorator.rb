Spree::Taxon.class_eval do
  def self.taxon_id_with_categories(product)
    taxons = joins(:products, :taxonomy).where(spree_taxonomies: { name: 'Categories' }, spree_products: { id: product.id })
    taxons.ids.first
  end
end
