module Spree::ProductDecorator
  def related_products
    Spree::Product.includes(master: [:images, :default_price]).in_taxons(taxons).distinct.order(updated_at: :desc).where.not(id: id)
  end

  Spree::Product.prepend self
end
