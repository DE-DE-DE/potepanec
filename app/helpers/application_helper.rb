module ApplicationHelper
  def full_title(unique_title)
    base_title = "BIGBAG Store"
    unique_title.present? ? "#{unique_title} - #{base_title}" : base_title
  end
end
